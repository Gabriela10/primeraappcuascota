//
//  infoViewController.swift
//  ToDo
//
//  Created by Gabriela Cuascota on 16/5/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import UIKit

class infoViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var itemInfo:(itemManager: ItemManagement, index: Int)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].title
        locationLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].location
        descriptionLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].description
    }
    
    @IBAction func CheckButtonPressed(_ sender: Any) {
        itemInfo?.itemManager.checkItem(index: (itemInfo?.index)!)
        navigationController?.popViewController(animated: true)
    }
}
