//
//  itemTableViewCell.swift
//  ToDo
//
//  Created by Gabriela Cuascota on 22/5/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import UIKit

class itemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
   
    @IBOutlet weak var locationLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    

}
